%% Analysis for GPS RTK rosbags
% Curtis Manore
% Note: columbus garage moving data has different topic names
close all;
format longG

sbagCol = rosbag('columbus_stationary.bag');
sbagIsec = rosbag('isec_stationary.bag');
mbagCol = rosbag('columbus_moving.bag');
mbagIsec = rosbag('isec_moving.bag');

% uncomment below and change titles for desired plots

%plotStationaryData(sbagCol)
%plotStationaryData(sbagIsec)

%plotMovingData(mbagCol, true)
plotMovingData(mbagIsec, false)

%% Moving Bag data
% columbus garage moving data was recorded with a different message type
function plotMovingData(mbag, isRooftop)
    % create conditional statement since rooftop data
    % has different topic names than isec data
    if isRooftop
        movingbSelCol = select(mbag,'Topic','/GNSS');
        movingmsgStructs = readMessages(movingbSelCol,'DataFormat','struct');
        
        lat = cellfun(@(m) double(m.Latitude),movingmsgStructs);
        lon = cellfun(@(m) double(m.Longitude),movingmsgStructs);
        alt = cellfun(@(m) double(m.Altitude),movingmsgStructs);
        
        utmEasting = cellfun(@(m) double(m.UtmEasting),movingmsgStructs);
        utmNorthing = cellfun(@(m) double(m.UtmNorthing),movingmsgStructs);
        
        fix = cellfun(@(m) int8(m.Quality),movingmsgStructs);
        
        time_roof = mbag.MessageList.Time(3:end); % in unix time
        
        timeElapsed = double((time_roof - min(time_roof)));
        ts = linspace(0,max(timeElapsed),length(timeElapsed));
    else
        movingbSelCol = select(mbag,'Topic','/gps');
        movingmsgStructs = readMessages(movingbSelCol,'DataFormat','struct');
        
        lat = cellfun(@(m) double(m.Lat),movingmsgStructs);
        lon = cellfun(@(m) double(m.Long),movingmsgStructs);
        alt = cellfun(@(m) double(m.Alt),movingmsgStructs);
    
        utmEasting = cellfun(@(m) double(m.UtmEasting),movingmsgStructs);
        utmNorthing = cellfun(@(m) double(m.UtmNorthing),movingmsgStructs);
        
        fix = cellfun(@(m) int8(m.Fix),movingmsgStructs);
        
        % get timestamp in nanoseconds
        Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), movingmsgStructs)*1e9;
        Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),movingmsgStructs);
        unix_ns = Sec + Nsec;
        
        timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
        ts = linspace(0,max(timeElapsed),length(timeElapsed));   
    end

    % do plotting and analyis:
    minEast = min(utmEasting);
    minNorth = min(utmNorthing);
    
    figure(3)
    subplot(1,2,1)
    % Only walked 30 meters, explain in report
    eastWalk = utmEasting-minEast;
    northWalk = utmNorthing-minNorth;
    startPointEast = eastWalk(1);
    startPointNorth = northWalk(1);
    endPointEast = eastWalk(end);
    endPointNorth = northWalk(end);
    %c = timeElapsed;
    scatter(eastWalk,northWalk,[],ts);
    xlabel('Distance Traveled East (m)')
    ylabel('Distance Traveled North (m)')
    title('Moving GPS Data - ISEC')
    hold on;
    scatter(startPointEast,startPointNorth,'magenta','p','filled','SizeData',300,'LineWidth',2)
    hold on;
    scatter(endPointEast,endPointNorth,'red','x','SizeData',300,'LineWidth',2)
    c = colorbar;
    c.Label.String = 'Time Elapsed (s)';
    legend('Measured Location', 'Start Point','End Point', Location='northwest')

    % find distance between start point and end point:
    startEnd = [startPointEast,startPointNorth;endPointEast,endPointNorth];
    d = pdist(startEnd,'euclidean')

    %figure(4)
    subplot(1,2,2)
    plot(ts,alt)
    meanAlt = mean(alt);
    yAlt = yline(meanAlt,'Color','r');
    axis([0 ts(end) min(alt)-0.5 max(alt)+0.5])
    yresid = alt - meanAlt;
    
    % squared error
    sqErr = yresid.^2;
    mse = mean(sqErr);
    RMSE = sqrt(mse);
    text = {[strcat(' MSE = ',sprintf('%.4f',mse))], ...
            [strcat('RMSE = ',sprintf('%.4f',RMSE))]};
    annotation('textbox',[.85, 0.55, 0.1, 0.1],'String',text)
    title('Altitude Walking - ISEC')
    legend('Measured Altitude','Mean Altitude')
    xlabel('Time (s)')
    ylabel('Altidude MSL (m)')


    figure(2)
    plot(ts,fix)
    title('Moving Fix Type - ISEC')
    xlabel('Time (s)')
    ylabel('Fix Type')
    axis([0 ts(end) 0 5.5])
    yticks([0 1 2 4 5])
    yticklabels({'0 - No Fix','1 - Single Point','2 - Pseudorange DGPS','4 - RTK Fixed','5 - RTK Floating'})

end

%% Stationary bag data
function plotStationaryData(sbag)
    stationbSel = select(sbag,'Topic','/gps');
    
    stationmsgStructs = readMessages(stationbSel,'DataFormat','struct');
    
    lat = cellfun(@(m) double(m.Lat),stationmsgStructs);
    lon = cellfun(@(m) double(m.Long),stationmsgStructs);
    alt = cellfun(@(m) double(m.Alt),stationmsgStructs);

    utmEasting = cellfun(@(m) double(m.UtmEasting),stationmsgStructs);
    utmNorthing = cellfun(@(m) double(m.UtmNorthing),stationmsgStructs);
    
    fix = cellfun(@(m) int8(m.Fix),stationmsgStructs);
    
    % get timestamp in nanoseconds
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), stationmsgStructs)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),stationmsgStructs);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
    
    meanEast = mean(utmEasting);
    meanNorth = mean(utmNorthing);
    mean(lat)
    mean(lon)
    %max(utmEasting)
    minEast = min(utmEasting);
    minNorth = min(utmNorthing);
    std(utmEasting)
    std(utmNorthing)
    spreadEast = utmEasting - minEast;
    spreadNorth = utmNorthing - minNorth;
    spreadMeanEast = meanEast - minEast;
    spreadMeanNorth = meanNorth - minNorth;
    
    figure
    subplot(1,2,1)
    % origin at (mineast, minnorth)
    scatter(spreadEast,spreadNorth,'filled')
    hold on;
    scatter(spreadMeanEast,spreadMeanNorth,'red','x','SizeData',300,'LineWidth',2)
    title('Stationary UTM Data Spread - ISEC')
    xlabel('UTM Easting Spread (m)')
    ylabel('UTM Northing Spread (m)')
    legend('Measured Location','Average Location',Location='southeast')
    hold off;
    
    %figure(2)
    subplot(1,2,2)
    plot(ts,alt)
    title('Stationary Altitude Drift - ISEC')
    xlabel('Time (s)')
    ylabel('Altidude MSL (m)')
    meanAlt = mean(alt);
    yAlt = yline(meanAlt,'Color','r');
    axis([0 ts(end) min(alt)-0.5 max(alt)+0.5])
    legend('Measured Altitude', 'Mean Altitude')
    %range(alt)
    % get residuals
    yresid = alt - meanAlt;
    
    % squared error
    sqErr = yresid.^2;
    mse = mean(sqErr);
    RMSE = sqrt(mse);
    text = {[strcat(' MSE = ',sprintf('%.4f',mse))], ...
            [strcat('RMSE = ',sprintf('%.4f',RMSE))]};
    
    annotation('textbox',[.85, 0.75, 0.1, 0.1],'String',text)
    %annotation('textbox',[.65, 0.2, 0.1, 0.1],'String',text)
    
    figure
    plot(ts,fix)
    title('Stationary Fix Type - ISEC')
    xlabel('Time (s)')
    ylabel('Fix Type')
    axis([0 ts(end) 0 5.5])
    yticks([0 1 2 4 5])
    yticklabels({'0 - No Fix','1 - Single Point','2 - Pseudorange DGPS','4 - RTK Fixed','5 - RTK Floating'})
    
end
