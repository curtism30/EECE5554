# Lab 4 Submission Notes
1. The analysis and the report is included under the analysis folder. To run the scripts, run part 1, then part 2, and finally part 3. If you would just like to skip to part 3, make sure the filtered yaw and linear velocity .mat files are included in your workspace. These are generated automatically from running parts 1 and 2.
2. The GPS and IMU drivers that ran on the car were developed by lab partner Andrew Goering and can be found under the gps-imu-drivers subdirectory.
3. In case we needed to make our own driver/launch file independent from the one used by the group, I included my own drivers located under the lab4 subdirectory.
