#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
import numpy as np
from sensor_msgs.msg import Imu, MagneticField
from geometry_msgs.msg import Quaternion, Vector3
from tf.transformations import quaternion_from_euler

'''
this driver publishes two topics:
- one for the imu positon data named imu/data
- one for the imu magnetometer data named imu/mag
'''

def yprToQuaternion(ypr):
    # math checked with this online calculator: https://www.andre-gaschler.com/rotationconverter/
    #yprRad = [math.radians(x) for x in ypr]
    yprRad = np.deg2rad(ypr)
    rpyRad = list(reversed(yprRad))
    quat = quaternion_from_euler(rpyRad[0],rpyRad[1],rpyRad[2])
    return Quaternion(quat[0],quat[1],quat[2],quat[3])

def gaussToTesla(magGauss):
    # 1 Tesla = 10,000 Gauss
    # divide by 10,000
    tesla = np.array(magGauss) / 10000
    return Vector3(tesla[0],tesla[1],tesla[2])


# return a list of IMU data in the form of:
# [quaternions, angular_velocities, linear_accelerations, magnetic_fields]
def getIMUData(line):
    # # get rid of header
    line = line.lstrip('\r$VNYMR,') 
    # grab everything infront of the * using partition
    (line, _, _) = line.partition('*')
    # split at commas and transform to a list
    line = line.split(',')
    # convert to float64
    line = [float(i) for i in line]

    # grab data fields
    ypr = line[0:3] # in degrees
    magGauss = line[3:6] # in Gauss
    accel = line[6:9]  # in m/s^2
    gyros = line[9:12] # in rad/s

    # do conversions
    quat = yprToQuaternion(ypr)
    magTesla = gaussToTesla(magGauss)
    gyros = Vector3(gyros[0],gyros[1],gyros[2])
    accel = Vector3(accel[0],accel[1],accel[2])

    return [quat, gyros, accel, magTesla]
    

if __name__ == '__main__':
    SENSOR_NAME = "imu"
    rospy.init_node('imu_node')

    # COMMENT OUT SERIAL PORTS NOT IN USE
    #serial_port = rospy.get_param('~port','/dev/ttyACM0')
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    #serial_port = rospy.get_param('~port','/dev/pts/2')

    serial_baud = rospy.get_param('~baudrate',115200)

    # sampling rate of IMU is 40 Hz
    sampling_rate = 40.0
    sleep_time = 1/sampling_rate - 0.025

    # create imu publishers
    # need two topic publishers, one for imu position data and another for mag
    imu_pub_data = rospy.Publisher(SENSOR_NAME+'/data', Imu, queue_size=10)
    imu_pub_mag = rospy.Publisher(SENSOR_NAME+'/mag', MagneticField, queue_size=10)

    imu_msg = Imu()
    imu_mag_msg = MagneticField()

    # set up headers
    imu_msg.header.frame_id = SENSOR_NAME
    imu_msg.header.seq = 0
    imu_mag_msg.header.frame_id = SENSOR_NAME
    imu_mag_msg.header.seq = 0

    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.loginfo("Using imu sensor on port "+serial_port+" at "+str(serial_baud))

    vnymr = bytes("$VNYMR", 'utf-8')
    #vnymr = bytes("\r$VNYMR", 'utf-8')

    try:
        while not rospy.is_shutdown():
            line = port.readline()

            if line == '':
                rospy.logwarn("IMU WARN: No data")
                           
            elif line.startswith(vnymr):

                line = line.decode("utf-8") 
                #rospy.loginfo("Found the line!")
                #rospy.loginfo(line)
                imuData = getIMUData(line)
                
                imu_msg.header.stamp=rospy.Time.now() 
                imu_msg.orientation = imuData[0]
                imu_msg.angular_velocity = imuData[1]
                imu_msg.linear_acceleration = imuData[2]

                imu_mag_msg.header.stamp=rospy.Time.now()
                imu_mag_msg.magnetic_field = imuData[3]

                imu_msg.header.seq+=0
                imu_mag_msg.header.seq+=0

                imu_pub_data.publish(imu_msg)
                imu_pub_mag.publish(imu_mag_msg)
                
            rospy.sleep(sleep_time)
            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down imu_node...")
