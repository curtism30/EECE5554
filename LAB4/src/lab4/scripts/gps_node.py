#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
import utm
import math
from std_msgs.msg import Float64
from lab1.msg import gps

# convert to degree decimal
def toDegreeDecimal(dms):
    # dms comes in to this function as dd.MMmm
    # seperate at the decimal and reformat
    (minutes, degree) = math.modf(dms)
    minutes = minutes*100
    return degree + minutes/60.0

'''
Gets all the data in the GNGGA line
and formats it into a list of values for the ros message
returns a list of format: 
[status, lattitude, longitude, altitude, utm_easting, utm_northing, Zone, letter]
'''
def getGPSData(line):
    #try:
    # need to convert lat and long to degree decimal
    lat = float(line[17:27].strip()) / 100.00 # full latitude
    lat = toDegreeDecimal(lat)
    
    # lat dir to string
    lat_dir = line[28:29]

    lon = float(line[30:40].strip()) / 100.00 # full longitude
    lon = toDegreeDecimal(lon)
    lon_dir = line[42:43]

    # fix type status
    status = int(line[44:45].strip()) # check if strip is needed for int

    # if statements to dictate whether lat and long is positive or negative
    if lat_dir == "S": lat = -lat
    if lon_dir == "W": lon = -lon
    #print(lon_dir)
    alt = float(line[54:58].strip()) # altitude

    # convert to utm
    (utm_easting, utm_northing, zone, letter) = utm.from_latlon(lat,lon)

    return [status, lat, lon, alt, utm_easting, utm_northing, zone, letter]

    #except: 
        #rospy.logwarn("Data exception: "+line)
        #return [0.0, 0.0, 0.0, 0.0, 0.0, 0, "None"]



if __name__ == '__main__':
    SENSOR_NAME = "gps"
    rospy.init_node('gps_node')

    # COMMENT OUT SERIAL PORT IF NOT USING SERIAL
    serial_port = rospy.get_param('~port','/dev/ttyACM0')
    #serial_port = rospy.get_param('~port','/dev/ttyUSB0')

    serial_baud = rospy.get_param('~baudrate',4800)

    # sampling rate of GPS is approx. 1 Hz by looking at incoming GPS data timestamps
    # set to 10 to ensure all data is received
    # data is broadcasted at 1 Hz in bag file
    sampling_rate = 10.0
    sleep_time = 1/sampling_rate - 0.025

    # create gps publisher
    gps_pub = rospy.Publisher(SENSOR_NAME+'/data', gps, queue_size=10)

    gps_msg = gps()

    # set up header
    gps_msg.header.frame_id = SENSOR_NAME
    gps_msg.header.seq = 0


    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.loginfo("Using gps sensor on port "+serial_port+" at "+str(serial_baud))


    gpgga = bytes("$GPGGA", 'utf-8')

    try:
        while not rospy.is_shutdown():
            line = port.readline()

            if line == '':
                rospy.logwarn("GPS WARN: No data")

            else:
                # if line starts with GPS line for lla data
                
                if line.startswith(gpgga):

                    line = line.decode("utf-8") 
                    rospy.loginfo("Found the line!")

                    gpsData = getGPSData(line)

                    #pucktimeLog = float(line[7:17].strip())
                    #rospy.loginfo(pucktimeLog)
                    
                    
                    gps_msg.header.stamp=rospy.Time.now() 

                    gps_msg.status = gpsData[0]
                    gps_msg.latitude = gpsData[1]
                    gps_msg.longitude = gpsData[2]
                    gps_msg.altitude = gpsData[3]
                    gps_msg.utm_easting = gpsData[4]
                    gps_msg.utm_northing = gpsData[5]
                    gps_msg.Zone = gpsData[6]
                    gps_msg.letter = gpsData[7]


                gps_msg.header.seq+=0
                gps_pub.publish(gps_msg)
                
            rospy.sleep(sleep_time)
            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down gps_node...")
