%% Lab 4 Magnetometer Calibration
% Correct Magnetometer readings for hard-iron and soft-iron
% and submit plots before and after correction
%print -deps epsFig


% import data
bag = rosbag('2022-03-04-09-21-00.bag');
magbSel = select(bag,'Topic','/vn100_mag');

magmsgStructs = readMessages(magbSel,'DataFormat','struct');

mag = cellfun(@(m) struct(m.MagneticField_),magmsgStructs);
magArray = [mag.X; mag.Y; mag.Z]';

imubSel = select(bag,'Topic','/vn100_imu');
imumsgStructs = readMessages(imubSel,'DataFormat','struct');
orientation = cellfun(@(m) struct(m.Orientation),imumsgStructs);
angVel = cellfun(@(m) struct(m.AngularVelocity),imumsgStructs);

% put quaternion in scalar first convention
quat = [orientation.W; orientation.X; orientation.Y; orientation.Z]';
%angVelArray = [angVel.X; angVel.Y; angVel.Z]';

% timeseries and time elapsed
[ts, timeElapsed] = getTimeSeries(magmsgStructs);

% GPS data for parsing
gpsbSel = select(bag,'Topic','/gps');

gpsmsgStructs = readMessages(gpsbSel,'DataFormat','struct');

lat = cellfun(@(m) double(m.Lat),gpsmsgStructs);
lon = cellfun(@(m) double(m.Long),gpsmsgStructs);
alt = cellfun(@(m) double(m.Alt),gpsmsgStructs);
[gps_ts, gpsTimeElapsed] = getTimeSeries(gpsmsgStructs);

%% Correct for hard iron and soft iron
close all;
% plot route
figure(1)
ax = gca;
geoscatter(lat,lon,[],gps_ts,'filled')
%geoscatter(lat(1:120),lon(1:120),[],gps_ts(1:120),'filled')

title('GPS Data and Route Overlay')
c = colorbar;
c.Label.String = 'Time Elapsed (s)';
hold on;
geoscatter(lat(1),lon(1),'magenta','p','filled','SizeData',300,'LineWidth',2)
hold on;
geoscatter(lat(end),lon(end),'red','x','SizeData',300,'LineWidth',2)
legend('GPS Data','Start Point','End Point', Location='northwest')
set(gca, 'ActivePositionProperty', 'position')
print -depsc route


% 275 to 380 seconds - circle at Ruggles
% plot before correction
Fs = 40; % Hz
iEnd = Fs*380;
%latCircle = lat(1:400);
%lonCircle = lon(1:400);
magCircle = [magArray(11000:iEnd,1), magArray(11000:iEnd,2), magArray(11000:iEnd,3)];

% try manual approach
%clf;
figure(2)
ax = gca;
scatter(magCircle(:,1),magCircle(:,2))
grid on;
hold on;
ax.Title.String = 'Magnetometer Ellipse Fitting - Ruggles Circle';
ax.XLabel.String = 'Mx (Gauss)';
ax.YLabel.String = 'My (Gauss)';
% ellipse fitting function from:
% https://www.mathworks.com/matlabcentral/fileexchange/3215-fit_ellipse
ellipse_t = fit_ellipse(magCircle(:,1),magCircle(:,2),ax)
print -depsc mag_ellipse

HI = [ellipse_t.X0_in; ellipse_t.Y0_in; 0];
scale = ellipse_t.short_axis / ellipse_t.long_axis;
theta = ellipse_t.phi;
SI = scale .* [cos(theta) sin(theta) 0;
               -sin(theta) cos(theta) 0];
SI = [SI; [0 0 1]];

magCorrected =  (magArray-HI')*SI;
magX = magCorrected(11000:iEnd,1);
magY = magCorrected(11000:iEnd,2);

figure(3)
%ax = gca()
scatter(magCircle(:,1),magCircle(:,2))
grid on;
title('Magnetic Measurements - Distortions and Corrections')
xlabel('Mx (Gauss)')
ylabel('My (Gauss)')
hold on;
%scatter(magCircleCorrected(:,1),magCircleCorrected(:,2))
%hold on; 
scatter(magX,magY);
%axis([-0.15 0.15 -0.3 0.3]);
axis('equal')
%axis([-0.1 0.1 -0.1 0.1]);
yline(0,'Color','k','LineWidth',1.5);
xline(0,'Color','k','LineWidth',1.5);
legend('With Distortions','After Corrections','','')
x0=50;
y0=50;
width=750;
height=600;
%ax.TitleHorizontalAlignment = 'center';
set(gcf,'position',[x0,y0,width,height])
print -depsc mag_corrections_hand

% use corrections for x and y only
%magCorrected = (magArray-b)*A;

% yaw angle from corrected mag data
yaw_mag = atan2(magCorrected(:,1),magCorrected(:,2));


%% Get yaw angle from IMU and compare
% use cumtrapz for now
[roll_truth, pitch_truth, yaw_truth] = quat2angle(quat);

% need to calculate a bias from first minute of stationary data:
start = Fs*60;
stop = Fs*120;
angbias_z = mean([angVel(start:stop).Z]);

% need to integrate and factor in the bias and initial quaternion
yaw_imu = wrapToPi(cumtrapz(ts,[angVel.Z]' - angbias_z) + yaw_truth(1));

% plot difference during ruggles circles
ts_plot = ts(11000:iEnd) - ts(11000);
figure(4)
plot(ts_plot,yaw_mag(11000:iEnd),ts_plot,yaw_imu(11000:iEnd),LineWidth=1.5);
hold on;
plot(ts_plot,yaw_truth(11000:iEnd),'--',LineWidth=2.5,Color='#7E2F8E');
axis([0 ts_plot(end) -4.2 4.2])
xlabel('Time Elapsed (Seconds)')
ylabel('Yaw Angle (Radians)')
title('Ruggles Circle Yaw Angle - Mag, Integrated Gyro, and Truth')
legend('Mag','Integrated Gyro','Quaternion Yaw Angle (Truth)')
x0=50;
y0=50;
width=1050;
height=900;
set(gcf,'position',[x0,y0,width,height])
print -depsc yawAngles_all

%% Create complementary filter
% source: https://www.pieter-jan.com/node/11

gain_mag = 0.02;
gain_imu = 0.98;
yaw_filtered = wrapToPi(gain_mag*unwrap(yaw_mag) + gain_imu*unwrap(yaw_imu));
figure(5)
plot(ts_plot,yaw_filtered(11000:iEnd),LineWidth=1.5,Color='#EDB120');
hold on;
plot(ts_plot,yaw_truth(11000:iEnd),'--',LineWidth=2,Color='#7E2F8E')
title('Complementary Filter Performance')
xlabel('Time Elapsed (Seconds)')
ylabel('Yaw Angle (Radians)')
legend('Filtered Yaw Angle','Quaternion Yaw Angle (Truth)')
axis([0 ts_plot(end) -4.0 4.0])
x0=50;
y0=50;
width=900;
height=750;
set(gcf,'position',[x0,y0,width,height])
print -depsc complementary_filter

%% Check performance over drive
start_driving = 480*Fs;
ts_drive = ts(start_driving:end) - ts(start_driving);

figure(6)
plot(ts_drive,yaw_filtered(start_driving:end), LineWidth = 1.5)
hold on;
plot(ts_drive,yaw_truth(start_driving:end), LineWidth = 1.5)
title('Filtered Yaw Performance While Driving')
xlabel('Time Elapsed (Seconds)')
ylabel('Yaw Angle (Radians)')
legend('Filtered Yaw Angle','Quaternion Yaw Angle (Truth)','Location','southeast')
%axis([0 ts_plot(end) -inf inf])
x0=50;
y0=50;
width=900;
height=750;
set(gcf,'position',[x0,y0,width,height])
print -depsc yaw_drive

%% Send filtered yaw to mat file
save('filtered_yaw.mat','yaw_filtered')

%print -depsc epsFig

function [ts, timeElapsed] = getTimeSeries(msgStruct)
    % get timestamps from headers with nanosecond precision
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), msgStruct)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),msgStruct);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
end