%% Part 3
%% import the data
bag = rosbag('2022-03-04-09-21-00.bag');
magbSel = select(bag,'Topic','/vn100_mag');

magmsgStructs = readMessages(magbSel,'DataFormat','struct');

mag = cellfun(@(m) struct(m.MagneticField_),magmsgStructs);
magArray = [mag.X; mag.Y; mag.Z]';

imubSel = select(bag,'Topic','/vn100_imu');
imumsgStructs = readMessages(imubSel,'DataFormat','struct');
orientation = cellfun(@(m) struct(m.Orientation),imumsgStructs);
angVel = cellfun(@(m) struct(m.AngularVelocity),imumsgStructs);
linAcc = cellfun(@(m) struct(m.LinearAcceleration),imumsgStructs);

% put quaternion in scalar first convention
quat = [orientation.W; orientation.X; orientation.Y; orientation.Z]';
%angVelArray = [angVel.X; angVel.Y; angVel.Z]';

% timeseries and time elapsed
[ts, timeElapsed] = getTimeSeries(magmsgStructs);

% GPS data for parsing
gpsbSel = select(bag,'Topic','/gps');

gpsmsgStructs = readMessages(gpsbSel,'DataFormat','struct');

lat = cellfun(@(m) double(m.Lat),gpsmsgStructs);
lon = cellfun(@(m) double(m.Long),gpsmsgStructs);
alt = cellfun(@(m) double(m.Alt),gpsmsgStructs);
utmEast = cellfun(@(m) double(m.UtmEasting),gpsmsgStructs);
utmNorth = cellfun(@(m) double(m.UtmNorthing),gpsmsgStructs);
[gps_ts, gpsTimeElapsed] = getTimeSeries(gpsmsgStructs);

% Grab filtered yaw and GPS yaw from part 1
load("filtered_yaw.mat")

% load forward velocity
load("forward_vel.mat")
ts_driving = ts_driving - ts_driving(1);

% parse yaw for driving bit, subtract difference in linvel integration time
start_driving = 480*40;

ts_yaw = ts(start_driving:end);
ts_yaw = ts_yaw - ts_yaw(1);
sampleDiff = length(ts_yaw) - length(ts_plot_imu);

yaw_filtered = yaw_filtered(start_driving:end);
yaw_filtered = yaw_filtered(1:end-sampleDiff);
%% Question 1
close all;
yaw_rate = [angVel.Z]';
yaw_rate = yaw_rate(start_driving:end-sampleDiff);
ts_yaw = ts_yaw(1:end-sampleDiff);

wxdot = yaw_rate.*linvel_imu;
% compare wxdot to yaccel
y_accel = [linAcc.Y]';
y_accel = y_accel(start_driving:end);

figure(1)
plot(ts_yaw,wxdot,LineWidth=2)
hold on;
plot(ts_driving,y_accel,LineWidth=0.2)
title('Linear Acceleration in Y direction')
l = legend('$\omega \dot{X}$','$\ddot{y}_{obs}$', 'Interpreter','latex');
ylabel('Linear Acceleration ($m/s^2$)','Interpreter', 'latex')
xlabel('Drive Time (seconds)','Interpreter', 'latex')
l.FontSize = 24;
print -depsc yAccel


%% Position estimate
% adjust the heading
yaw_filtered = yaw_filtered + 0.2;
V_east = -1 * linvel_imu .* cos(yaw_filtered);
V_north = linvel_imu .* sin(yaw_filtered);

% check for bias, might need to subtract here
pos_x = cumtrapz(ts_plot_imu,V_east);
pos_y = cumtrapz(ts_plot_imu,V_north);

% set up ground truth
ts_plot_gps = gps_ts(480:end);
utmEast_drive = utmEast(480:end);
utmNorth_drive = utmNorth(480:end);

truth_east = utmEast_drive - utmEast_drive(1);
truth_north = utmNorth_drive - utmNorth_drive(1);

figure(2)
scatter(pos_x,pos_y)
hold on;
scatter(truth_east,truth_north)
hold on; 
scatter(pos_x(1),pos_y(1),'magenta','p','filled','SizeData',300,'LineWidth',2)
hold on;
scatter(pos_x(end),pos_y(end),'green','x','SizeData',300,'LineWidth',2)

legend('Dead Reckoning Position', 'GPS Ground Truth','DR Start Location','DR End Location','Location','southwest')
title('Dead Reckoning Position Estimate')
xlabel('X-direction Displacement (meters)')
ylabel('Y-direction Displacement (meters)')
print -depsc deadReckon


%% Estimate Xc
wdot = diff(yaw_rate);
A = -1*wdot;
B = wxdot(1:(end-1));
Xc_mat = linsolve(A,B);
Xc = mean(Xc_mat)

function [ts, timeElapsed] = getTimeSeries(msgStruct)
    % get timestamps from headers with nanosecond precision
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), msgStruct)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),msgStruct);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
end