%% Part 2 Forward Velocity Estimate
% import the data
bag = rosbag('2022-03-04-09-21-00.bag');

imubSel = select(bag,'Topic','/vn100_imu');
imumsgStructs = readMessages(imubSel,'DataFormat','struct');
linAcc = cellfun(@(m) struct(m.LinearAcceleration),imumsgStructs);

gpsbSel = select(bag,'Topic','/gps');
gpsmsgStructs = readMessages(gpsbSel,'DataFormat','struct');
utmEast = cellfun(@(m) double(m.UtmEasting),gpsmsgStructs);
utmNorth = cellfun(@(m) double(m.UtmNorthing),gpsmsgStructs);

% timeseries and time elapsed
[ts, timeElapsed] = getTimeSeries(imumsgStructs);
[gps_ts, gpsTimeElapsed] = getTimeSeries(gpsmsgStructs);

%% Get forward velocity from acceleration
forward_accel = [linAcc.X];

% get forward acceleration bias from start of collect
Fs = 40;
start = Fs*60;
stop = Fs*120;
linbias_x = mean([linAcc(start:stop).X]);

% parse for the start of the driving segment: start at 500 seconds
start_driving = 480*40;
forward_accel = forward_accel(start_driving:end)';
ts_driving = ts(start_driving:end);

% do integration
linvel_imu = cumtrapz(ts_driving,forward_accel - linbias_x);

%% GPS forward velocity
% do derivative for this, distance traveled / time 
% (difference per time step)
Fgps = 1;
linvel_gps = utmToDistance(utmEast,utmNorth) / Fgps;
% parse for driving segment
linvel_gps = linvel_gps(480:end);
gps_ts = gps_ts(480:end-1);

%% Plot
close all;
ts_plot_imu = ts_driving - ts_driving(1);
ts_plot_gps = gps_ts - gps_ts(1);

figure(1)
% plot acceleration
plot(ts_plot_imu,forward_accel)
axis([0 ts_plot_gps(end) -inf inf])
title('Forward Acceleration')
xlabel('Time Elapsed (Seconds)','Interpreter', 'latex')
ylabel('Linear Acceleration ($m/s^2$)','Interpreter', 'latex')
print -depsc accel


figure(2)
plot(ts_plot_imu,linvel_imu,ts_plot_gps,linvel_gps,LineWidth=1.5)
legend('Forward Velocity - IMU','Forward Velocity - GPS')
axis([0 ts_plot_gps(end) -inf inf])
title('Forward Velocity Comparison - No Adjustments')
xlabel('Time Elapsed (Seconds)')
ylabel('Linear Velocity (m/s)')
print -depsc forwardVelBeforeAdjust



%% Linear Velocity Calculation Improvements
% grab stops, calculate bias between each stop interval
% integrate between each interval and subtract the calculated bias
stopTimes = [1,90,175,225,271,370,633,699,744,786,879,978];
% convert stop times in seconds to sample times
stopTimes = stopTimes .* Fs;

% create acceleration cell array that is sliced into intervals
% each cell is a time interval
% ex, cell 1 is interval 0-90 seconds, cell 2 90-175
accel_new = {};
ts_new = {};
biases = [];
for i=1:length(stopTimes)
    if i ~= length(stopTimes)
        accel_new(i,:) = {forward_accel(stopTimes(i):stopTimes(i+1))};
        ts_new(i,:) = {ts_plot_imu(stopTimes(i):stopTimes(i+1))};
    else
        % if last element
        accel_new(i,:) = {forward_accel(stopTimes(i):length(forward_accel))};
        ts_new(i,:) = {ts_plot_imu(stopTimes(i):length(forward_accel))};
    end
    if i == 1
        % if first index use precomputed, starting bias
        biases = [linbias_x];
        continue
    end
    bias = mean(accel_new{i,:});
    biases = [biases;bias];
end

% do integration over each integral and combine to one array
linvel_array = [];
ts_final = [];
for i=1:length(accel_new)
    temp = cumtrapz(ts_new{i,:},accel_new{i,:}-biases(i));
    linvel_array = [linvel_array;temp];
    ts_final = [ts_final, ts_new{i,:}];
end
negIdx = linvel_array < 0;
linvel_array(negIdx) = 0;

figure(3)
plot(ts_final,linvel_array,ts_plot_gps,linvel_gps,LineWidth=1.5)
legend('Forward Velocity - IMU','Forward Velocity - GPS')
axis([0 ts_plot_gps(end) -inf inf])
title('Forward Velocity Comparison - After Adjustments')
xlabel('Time Elapsed (Seconds)')
ylabel('Linear Velocity (m/s)')

print -depsc forwardVelAfterAdjust


linvel_imu = linvel_array;
ts_plot_imu = ts_final;
save('forward_vel.mat','linvel_imu','ts_plot_imu','linvel_gps','ts_plot_gps','ts_driving')


function dist = utmToDistance(utmEast,utmNorth)
    dist = sqrt(diff(utmEast).^2+diff(utmNorth).^2);
end

function [ts, timeElapsed] = getTimeSeries(msgStruct)
    % get timestamps from headers with nanosecond precision
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), msgStruct)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),msgStruct);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
end