%% Analysis for GPS rosbags
% Curtis Manore
%% Stationary bag data
close all;
format longG

sbag = rosbag('stationary_gps.bag');
stationbSel = select(sbag,'Topic','/gps/data');

stationmsgStructs = readMessages(stationbSel,'DataFormat','struct');

lat = cellfun(@(m) double(m.Latitude),stationmsgStructs);
lon = cellfun(@(m) double(m.Longitude),stationmsgStructs);
alt = cellfun(@(m) double(m.Altitude),stationmsgStructs);


utmEasting = cellfun(@(m) double(m.UtmEasting),stationmsgStructs);
utmNorthing = cellfun(@(m) double(m.UtmNorthing),stationmsgStructs);

% get timestamp in nanoseconds
Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), stationmsgStructs)*1e9;
Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),stationmsgStructs);
unix_ns = Sec + Nsec;

timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
ts = linspace(0,max(timeElapsed),length(timeElapsed));

% google earth utm location:
%gEasting = 328142.14;
%gNorthing = 4689488.67;

meanEast = mean(utmEasting);
meanNorth = mean(utmNorthing);

%max(utmEasting)
minEast = min(utmEasting);
minNorth = min(utmNorthing);

spreadEast = utmEasting - minEast;
spreadNorth = utmNorthing - minNorth;
spreadMeanEast = meanEast - minEast;
spreadMeanNorth = meanNorth - minNorth;

figure(1)
subplot(1,2,1)
% origin at (mineast, minnorth)
scatter(spreadEast,spreadNorth,'filled')
hold on;
scatter(spreadMeanEast,spreadMeanNorth,'red','x','SizeData',300,'LineWidth',2)
title('Stationary UTM Data Spread')
xlabel('UTM Easting Spread (m)')
ylabel('UTM Northing Spread (m)')
legend('Measured Location','Average Location')
hold off;

%figure(2)
subplot(1,2,2)
plot(ts,alt)
title('Stationary Altitude Drift')
xlabel('Time (s)')
ylabel('Altidude MSL (m)')
meanAlt = mean(alt);
yAlt = yline(meanAlt,'Color','r');
axis([0 600 20 30])
legend('Measured Altitude', 'Mean Altitude')
%range(alt)
% get residuals
yresid = alt - meanAlt;

% squared error
sqErr = yresid.^2;
mse = mean(sqErr);
RMSE = sqrt(mse);
text = {[strcat(' MSE = ',sprintf('%.4f',mse))], ...
        [strcat('RMSE = ',sprintf('%.4f',RMSE))]};

annotation('textbox',[.80, 0.15, 0.1, 0.1],'String',text)
%annotation('textbox',[.65, 0.2, 0.1, 0.1],'String',text)

%% Moving Bag data
mbag = rosbag('moving_gps.bag');
movingbSel = select(mbag,'Topic','/gps/data');

movingmsgStructs = readMessages(movingbSel,'DataFormat','struct');

lat = cellfun(@(m) double(m.Latitude),movingmsgStructs);
lon = cellfun(@(m) double(m.Longitude),movingmsgStructs);
alt = cellfun(@(m) double(m.Altitude),movingmsgStructs);


utmEasting = cellfun(@(m) double(m.UtmEasting),movingmsgStructs);
utmNorthing = cellfun(@(m) double(m.UtmNorthing),movingmsgStructs);

% get timestamp in nanoseconds
Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), movingmsgStructs)*1e9;
Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),movingmsgStructs);
unix_ns = Sec + Nsec;

timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
ts = linspace(0,max(timeElapsed),length(timeElapsed));

minEast = min(utmEasting);
minNorth = min(utmNorthing);

figure(2)
subplot(1,2,1)
% Only walked 30 meters, explain in report
eastWalk = utmEasting-minEast;
northWalk = utmNorthing-minNorth;
scatter(eastWalk,northWalk,'filled')
xlabel('Distance Traveled East (m)')
ylabel('Distance Traveled North (m)')
title('Moving GPS Data - Straight Line')

% add linear regression, straight line fit
hold on
b1 = eastWalk \ northWalk;
yCalc1 = b1*eastWalk;
plot(eastWalk,yCalc1,'LineWidth',2)
legend('Measured Location','Line of Best Fit','Location','northwest')

% fit analysis
% R^2
Rsq1 = 1 - sum((northWalk - yCalc1).^2)/sum((northWalk - mean(northWalk)).^2);

% get residuals
yresid = northWalk - yCalc1;

% squared error
sqErr = yresid.^2;
mse = mean(sqErr);
RMSE = sqrt(mse);

text = {[strcat('  R^{2} = ',sprintf('%.4f',Rsq1))], ...
        [strcat(' MSE = ',sprintf('%.4f',mse))], ...
        [strcat('RMSE = ',sprintf('%.4f',RMSE))]};
annotation('textbox',[.35, 0.15, 0.1, 0.1],'String',text)


%figure(4)
subplot(1,2,2)
plot(ts,alt)
title('Altitude Walking Down Ramp')
hold on;

% use curve fitting toolbox to map a linear regression line
% data is not as linear, so toolbox is used

[f, stats] = fit(timeElapsed,alt,'poly1');
plot(f)
legend('Measured Altitude','Line of Best Fit')
xlabel('Time (s)')
ylabel('Altidude MSL (m)')
axis([0 92 25.5 30])
text = {[strcat('  R^{2} = ',sprintf('%.4f',stats.rsquare))], ...
        [strcat(' MSE = ',sprintf('%.4f',(stats.rmse)^2))], ...
        [strcat('RMSE = ',sprintf('%.4f',stats.rmse))]};

annotation('textbox',[.60, 0.15, 0.1, 0.1],'String',text)
