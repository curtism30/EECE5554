# Lab 5
The main stitching and panorama-making code is located in the *stitch.m* file. The *harris.m* and *convolve2.m* files were not modified. The writeup is also located in the main directory under *lab5_report.pdf*.

### Sub Directories
- TOOLBOX_calib: contains the calibration toolbox from Caltech.
- brick_imgs: contains the images of the brick wall.
- calibration_imgs: contains the images used for calibration
- large_mural_imgs: contains the mural images with 15% overlap
- mural_imgs: contains the images for the original mural with 50% overlap.
- outputs: contains the outputs for calibration and mosaics, including features detected, feature matches, and resulting mosaics.
