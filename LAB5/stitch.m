%% Image stitching function
% Curtis Manore
% Based on template from matlab: 
% https://www.mathworks.com/help/vision/ug/feature-based-panoramic-image-stitching.html?searchHighlight=panorama&s_tid=doc_srchtitle
close all; clear;

% Load images.
muralDir = fullfile('/home/curtis/classes/EECE5554/LAB5','brick_imgs','IMG_80*');
muralScene = imageDatastore(muralDir);

% Display images to be stitched.
figure(1)
montage(muralScene.Files,'Size',[3 2])

% Read the first image from the image set.
I = readimage(muralScene,1);

% undistort image using calibration
% see function description at bottom 
I = undistortImageWithCal(I);


% Need to do image pre-processing'
% send to preprocessing function
I = imgPreProc(I,'GrayImage','True');

% Initialize features for I(1)
% harrisDetector is my helper function for harris (see function at bottom)
[y,x,m] = harrisDetector(I);
points = [x,y];
[features, points] = extractFeatures(I,points);

%figure(2)
%imshow(I)

% iterative initializations
numImages = numel(muralScene.Files);
tforms(numImages) = projective2d(eye(3));

% Initialize variable to hold image sizes.
imageSize = zeros(numImages,2);

% Iterate over remaining image pairs
for n = 2:numImages
    
    % Store points and features for I(n-1).
    pointsPrevious = points;
    featuresPrevious = features;
        
    % Read I(n) and pre-process.
    Img_new = readimage(muralScene, n);
    Img_new = undistortImageWithCal(Img_new);
    grayImage = imgPreProc(Img_new,'GrayImage','True');   
    
    % Save image size.
    imageSize(n,:) = size(grayImage);
    
    % Detect and extract SURF features for I(n).
    [y,x,m] = harrisDetector(grayImage);
    points = [x,y];
    [features, points] = extractFeatures(grayImage,points);

    % Find correspondences between I(n) and I(n-1).
    indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);

    matchedPoints = points(indexPairs(:,1), :);
    matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        

    % set I to previous image
    I = grayImage;

    % Estimate the transformation between I(n) and I(n-1).
    [tforms(n),inlierIdx] = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
        'projective', 'Confidence', 99.9, 'MaxNumTrials', 10000);

    inlierPtsPrev = matchedPointsPrev(inlierIdx,:);
    inlierPts = matchedPoints(inlierIdx,:);
    
    % plot all matches vs inlier matches
    figure(n)
    subplot(1,2,1)
    showMatchedFeatures(I,grayImage,matchedPointsPrev,matchedPoints)
    legend('matched points 1','matched points 2');
    title('Matched Points before Outlier Rejection')

    subplot(1,2,2)
    showMatchedFeatures(I,grayImage,inlierPtsPrev,inlierPts)
    legend('matched points 1','matched points 2');
    title('Inlier Points after Outlier Rejection')
    set(gcf,'position',[500,500,1800,1200])

    % Compute T(n) * T(n-1) * ... * T(1)
    tforms(n).T = tforms(n).T * tforms(n-1).T; 
end


%% Stitch the panaroma
% Compute the output limits for each transform.
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);    
end

avgXLim = mean(xlim, 2);
[~,idx] = sort(avgXLim);
centerIdx = floor((numel(tforms)+1)/2);
centerImageIdx = idx(centerIdx);

Tinv = invert(tforms(centerImageIdx));
for i = 1:numel(tforms)    
    tforms(i).T = tforms(i).T * Tinv.T;
end

% initialize the panorama
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
end

maxImageSize = max(imageSize);

% Find the minimum and maximum output limits. 
xMin = min([1; xlim(:)]);
xMax = max([maxImageSize(2); xlim(:)]);

yMin = min([1; ylim(:)]);
yMax = max([maxImageSize(1); ylim(:)]);

% Width and height of panorama.
width  = round(xMax - xMin);
height = round(yMax - yMin);

% Initialize the "empty" panorama.
panorama = zeros([height width 3], 'like', I);

% create the panorama and build the scene
blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');  

% Create a 2-D spatial reference object defining the size of the panorama.
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);

% Create the panorama.
for i = 1:numImages
    
    I = readimage(muralScene, i);
    I = imgPreProc(I);
   
    % Transform I into the panorama.
    warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);
                  
    % Generate a binary mask.    
    mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);
    
    % Overlay the warpedImage onto the panorama.
    panorama = step(blender, panorama, warpedImage, mask);
end

figure(9)
imshow(panorama)



% harrisDetector is a helper function that allows me to tune parameters for
% harris.m in one location (as opposed to scattered throughout the code)
function [y,x,m] = harrisDetector(I)
    iSize = size(I);
    mask = ones(iSize);
    [y,x,m] = harris(I,8000,'tile',[20 20],'disp','mask',mask,'fft','eig');
    %[y,x,m] = harris(I,4000,'tile',[8 8]);
end


% undistortImageWithCal is a function that undistorts the image
% given the camera parameters from the calibration toolbox
function img_undistort = undistortImageWithCal(I)
    IntrinsicMatrix = [2990.31724 0 0; 0 2991.53593 0; 2016 1512 1];
    radialDistortion = [0.10877 -0.16874]; 
    tanDistortion = [-0.00058 -0.00116]';
    cameraParams = cameraParameters('IntrinsicMatrix',IntrinsicMatrix,'RadialDistortion',radialDistortion,'TangentialDistortion',tanDistortion);
    img_undistort = undistortImage(I,cameraParams);
end

function img_out = imgPreProc(I,varargin)
    img_process = imrotate(I,-90);
    img_out = imresize(img_process,0.5);
    if ~isempty(varargin) & varargin{2} == 'True'
        img_out = im2gray(img_out);
        img_out = adapthisteq(img_out, 'clipLimit',0.02,'Distribution','rayleigh');;
    end
end