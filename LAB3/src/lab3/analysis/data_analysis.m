%% IMU Data Analysis
% This script plots the timeseries data for
% 3 accelerometers, 3 angular rate gyros,
% and 3- axis magnetometers and returns the mean and std

close all;
format longG;

%% import the data
bag = rosbag('imu_data_short.bag');
imubSel = select(bag,'Topic','/imu/data');
magbSel = select(bag,'Topic','/imu/mag');

imumsgStructs = readMessages(imubSel,'DataFormat','struct');
magmsgStructs = readMessages(magbSel,'DataFormat','struct');

linAcc = cellfun(@(m) struct(m.LinearAcceleration),imumsgStructs);
angVel = cellfun(@(m) struct(m.AngularVelocity),imumsgStructs);
mag = cellfun(@(m) struct(m.MagneticField_),magmsgStructs);

% get timeseries data
tsImu = getTimeSeries(imumsgStructs);
tsMag = getTimeSeries(magmsgStructs);

%% Analyze the data
% reshape IMU and mag data into an array
linAccArray = [linAcc.X; linAcc.Y; linAcc.Z]';
angVelArray = [angVel.X; angVel.Y; angVel.Z]';
magArray = [mag.X; mag.Y; mag.Z]';

% turn into timeseries data for mean and std calcs
linearTimeData = timeseries(linAccArray,tsImu);
angVelTimeData = timeseries(angVelArray,tsImu);
magTimeData = timeseries(magArray,tsMag);

% compute the mean values
linAvg = mean(linearTimeData)
angAvg = mean(angVelTimeData)
magAvg = mean(magTimeData)

% compute the standard deviation
linstd = std(linearTimeData)
angstd = std(angVelTimeData)
magstd = std(magTimeData)

% grab the accelerometer and gyroscope measurement noise standard deviation
% this value is the max standard deviation between the acc/gyro axes
% commonly used for parameterizing noise for VIO algorithms
acc_n = max(linstd)
gyr_n = max(angstd)

%% Plot the data
% Linear Acceleration
figure(1)
subplot(3,1,1)
plot(linearTimeData.Data(:,1), Color='#0072BD')
axis([0 tsImu(end) -inf inf])
yline(linAvg(1),'k--',LineWidth=1.5);
title('Linear Acceleration - X')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,2)
plot(linearTimeData.Data(:,2), Color='#D95319')
axis([0 tsImu(end) -inf inf])
yline(linAvg(2),'k--',LineWidth=1.5);
title('Linear Acceleration - Y')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,3)
plot(linearTimeData.Data(:,3), Color='#EDB120')
axis([0 tsImu(end) -inf inf])
yline(linAvg(3),'k--',LineWidth=1.5);
title('Linear Acceleration - Z')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured','Mean',Location='southeast')

% Angular Velocity
figure(2)
subplot(3,1,1)
plot(angVelTimeData.Data(:,1), Color='#0072BD')
axis([0 tsImu(end) -inf inf])
yline(angAvg(1),'k--',LineWidth=1.5);
title('Angular Velocity - X')
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,2)
plot(angVelTimeData.Data(:,2), Color='#D95319')
axis([0 tsImu(end) -inf inf])
yline(angAvg(2),'k--',LineWidth=1.5);
title('Angular Velocity - Y')
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,3)
plot(angVelTimeData.Data(:,3), Color='#EDB120')
axis([0 tsImu(end) -inf inf])
yline(angAvg(3),'k--',LineWidth=1.5);
title('Angular Velocity - Z')
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
legend('Measured','Mean',Location='southeast')

% Magnetic Field
figure(3)
subplot(3,1,1)
plot(magTimeData.Data(:,1), Color='#0072BD')
axis([0 tsMag(end) -inf inf])
yline(magAvg(1),'k--',LineWidth=1.5);
title('Magnetic Field - X')
xlabel('Time (s)')
ylabel('Magnetic Field (T)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,2)
plot(magTimeData.Data(:,2), Color='#D95319')
axis([0 tsMag(end) -inf inf])
yline(magAvg(2),'k--',LineWidth=1.5);
title('Magnetic Field - Y')
xlabel('Time (s)')
ylabel('Magnetic Field (T)')
legend('Measured','Mean',Location='southeast')

subplot(3,1,3)
plot(magTimeData.Data(:,3), Color='#EDB120')
axis([0 tsMag(end) -inf inf])
yline(magAvg(3),'k--',LineWidth=1.5);
title('Magnetic Field - Z')
xlabel('Time (s)')
ylabel('Magnetic Field (T)')
legend('Measured','Mean',Location='southeast')

function ts = getTimeSeries(msgStruct)
    % get timestamps from headers with nanosecond precision
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), msgStruct)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),msgStruct);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
end