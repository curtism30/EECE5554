%% Allan Variance Analysis
% Allan variance analysis of gyroscope and accelerometer
close all;

% import the data:
bag = rosbag('allanbag.bag');
imubSel = select(bag,'Topic','/imu');

imumsgStructs = readMessages(imubSel,'DataFormat','struct');

linAcc = cellfun(@(m) struct(m.LinearAcceleration),imumsgStructs);
angVel = cellfun(@(m) struct(m.AngularVelocity),imumsgStructs);

% reshape IMU and mag data into an array
linAccArray = [linAcc.X; linAcc.Y; linAcc.Z]';
angVelArray = [angVel.X; angVel.Y; angVel.Z]';

% get timeseries data
tsImu = getTimeSeries(imumsgStructs);

%% Gyroscope Allan Variance
close all;

[Ng,Kg,Bg,tauParams_g,params_g] = plotAllanVar(angVelArray)
axis([-inf inf -inf inf])
title('Gyroscope Allan Deviation with Noise Parameters')
xlabel('\tau (s)')
ylabel('\sigma(\tau) (rad/s)')
legend('$\sigma_x (rad/s)$', '$\sigma_y (rad/s)$', '$\sigma_z (rad/s)$',...
    '$\sigma_N ((rad/s)/\sqrt{Hz})$', '$\sigma_K ((rad/s)\sqrt{Hz})$', ...
    '$\sigma_B (rad/s)$', 'Interpreter', 'latex')
text(tauParams_g, params_g, {'\bf{\ N}', '\bf{\ K}', '\bf{\ 0.664B}'},'Interpreter', 'latex')
grid on

%% Accelerometer Allan Variance
figure(2)
[Na,Ka,Ba,tauParams_a,params_a] = plotAllanVar(linAccArray)
axis([-inf inf -inf inf])
title('Accelerometer Allan Deviation with Noise Parameters')
xlabel('\tau (s)')
ylabel('\sigma(\tau) (m/s^2)')
legend('$\sigma_x (m/s^2)$', '$\sigma_y (m/s^2)$', '$\sigma_z (m/s^2)$',...
    '$\sigma_N ((m/s^2)/\sqrt{Hz})$', '$\sigma_K ((m/s^2)\sqrt{Hz})$', ...
    '$\sigma_B (m/s^2)$', 'Interpreter', 'latex')
text(tauParams_a, params_a, {'\bf{\ N}', '\bf{\ K}', '\bf{\ 0.664B}'},'Interpreter', 'latex')
grid on


%% Bias instability noise parameter:
Fs = 40; % Hz
t0 = 1/Fs;
acc_w = sqrt(t0/tauParams_a(3))*Ba
gyr_w = sqrt(t0/tauParams_g(3))*Bg


function [N,K,B,tauParams,params] = plotAllanVar(dataArray)
    % set frequency
    Fs = 40; % Hz
    t0 = 1/Fs;
    theta = cumsum(dataArray, 1)*t0;
    
    % grab values for tau
    maxNumM = 100;
    L = size(theta, 1);
    maxM = 2.^floor(log2(L/2));
    m = logspace(log10(1), log10(maxM), maxNumM).';
    m = ceil(m); % m must be an integer.
    m = unique(m); % Remove duplicates.
    
    % get avar and adev
    [avar, tau] = allanvar(dataArray,m,Fs);
    adev = sqrt(avar);
    
    % get angle random walk noise for each direction
    [lineNx, tauN, Nx] = angleRandomWalk(adev(:,1),tau);
    [lineNy, ~, Ny] = angleRandomWalk(adev(:,2),tau);
    [lineNz, ~, Nz] = angleRandomWalk(adev(:,3),tau);
    lineN = [lineNx,lineNy,lineNz];
    
    % angle walk noise parameter is the worst (max) noise of the three axes
    [N,i] = max([Nx,Ny,Nz]);
    lineN = lineN(:,i);
    
    % get rate random walk noise for each direction
    [lineKx, tauK, Kx] = rateRandomWalk(adev(:,1),tau);
    [lineKy, ~, Ky] = rateRandomWalk(adev(:,2),tau);
    [lineKz, ~, Kz] = rateRandomWalk(adev(:,3),tau);
    lineK = [lineKx,lineKy,lineKz];
    
    % rate random walk noise parameter
    [K,i] = max([Kx,Ky,Kz]);
    lineK = lineK(:,i);
    
    % get bias instability for each direction
    [lineBx, tauBx, Bx, scfB] = biasInstability(adev(:,1),tau);
    [lineBy, tauBy, By, ~] = biasInstability(adev(:,2),tau);
    [lineBz, tauBz, Bz, ~] = biasInstability(adev(:,3),tau);
    lineB = [lineBx,lineBy,lineBz];
    
    % bias instability noise parameter
    [B,i] = max([Bx,By,Bz]);
    tauB = [tauBx,tauBy,tauBz];
    tauB = tauB(i);
    lineB = lineB(:,i);
    
    tauParams = [tauN, tauK, tauB];
    params = [N, K, scfB*B];
    loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
        tauParams, params, 'o',LineWidth=2)


end


%% Noise Parameter Functions
% Angle random walk
function [lineN, tauN, N] = angleRandomWalk(adevSingle,tau)
    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = -0.5;
    logtau = log10(tau);
    logadev = log10(adevSingle);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the angle random walk coefficient from the line.
    logN = slope*log(1) + b;
    N = 10^logN;
    
    tauN = 1;
    lineN = N ./ sqrt(tau);
end 

% Rate random walk
function [lineK, tauK, K] = rateRandomWalk(adevSingle,tau)
    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0.5;
    logtau = log10(tau);
    logadev = log10(adevSingle);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the rate random walk coefficient from the line.
    logK = slope*log10(3) + b;
    K = 10^logK;
    
    tauK = 3;
    lineK = K .* sqrt(tau/3);
end

% Bias Instability
function [lineB, tauB, B, scfB] = biasInstability(adevSingle,tau)
    % Find the index where the slope of the log-scaled Allan deviation is equal
    % to the slope specified.
    slope = 0;
    logtau = log10(tau);
    logadev = log10(adevSingle);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    disp(min(abs(dlogadev - slope)))
    % Find the y-intercept of the line.
    b = logadev(i) - slope*logtau(i);
    
    % Determine the bias instability coefficient from the line.
    scfB = sqrt(2*log(2)/pi);
    logB = b - log10(scfB);
    B = 10^logB;
    
    tauB = tau(i);
    lineB = B * scfB * ones(size(tau));
end

% timeseries function
function ts = getTimeSeries(msgStruct)
    % get timestamps from headers with nanosecond precision
    Sec = cellfun(@(m) uint64(m.Header.Stamp.Sec), msgStruct)*1e9;
    Nsec = cellfun(@(m) uint64(m.Header.Stamp.Nsec),msgStruct);
    unix_ns = Sec + Nsec;
    
    timeElapsed = double((unix_ns - min(unix_ns)))/1.0e9;
    ts = linspace(0,max(timeElapsed),length(timeElapsed));
end