# README for PA1
This is the complete package for Curtis Manore's PA1. 

I included a launch file that runs the publisher and subscriber nodes. The CMakeList and package.xml files were also stripped of default comments and modified for the specific package.
